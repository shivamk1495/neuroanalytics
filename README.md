# Neuroanalytics

This repository contains all of the Homework* directories which further contains several bash (.sh) and R (.R) scripts. All of the files are created for the learning purpose and are intend to change every week.

Below is a little overview of the work upto now

---

## Homework1

This directory holds 4 assignments as below

1. `assignment1` is a basic pattern making script which make an illusion of loading/processing something.
2. `assignment2` is a file reading example. This reads `hapmap1.ped` file line by line and echo it.
3. `assignment3` creates a 50 donors sample data with 10 time points for each donor.
4. `assignment4` while creating sample donors data a lot of files has been created in `assignment3` which doesn't look much good. This will organize this messy data in several directories in a `fakedata` directory.

---

## Homework2

Homework2 directory have 3 assignment as shown below

1. `assignemnt1` is a simple text files which explains how the `.gitignore` & `README` files are added to this repository.
2. `assignment2` is much similar to assignment 3 which creates 100 donors sample data files, just with a tweak in the name of those data files.
3. `assignment3` is a simple R script which have a sample message to Jordan and it echos that message.

---

## Homework3

Homework3 directory have 2 assignment as shown below

1. `assignemnt1` has the R script, which plots the data of 100 donors present in fakedata.  
2. `assignment2` contain a script which plots the genotype and phenotype from hapmap1.ped file and qt.phe.

---

## Homework4

Homework4 directory have 3 assignment as shown below

1. `assignemnt1` has the R script, which filtered nomissing genotype whose thresholds is greater than 5 %.  
2. `assignment2` contain a script which filters allele frequency less than 5% or greater than 95%.
3. `assignment3` is a simple R script which plots nomissing genotype with frequency less than 5% or greater than 95% data.

## Homework5

Homework5 directory have 4 assignment as shown below

1. `assignemnt1` includes a R script that generates a histogram of the phenotypic values contained in the qt.phe file.  
2. `assignment2` includes a script called genohisto.R that generates a histogram of minor allele frequencies from all SNPs in hapmap1 nomissing AF.ped/.map.
3. `assignment3` R script is used to plots the SNPs on chromosome 4 with P-values from the UK Biobank project's dataset for frequency of drinking alcohol and saves the plot as AlcoholManhattan.pdf.
4. `assignment4` contains a paragraph summary of what I want to do for my final project in the form of a.pdf file.

## Homework6

Homework6 directory have 2 assignment as shown below

1. `assignemnt1` includes couple of R scripts and batch (Slurm) script that generates a histogram after runnning linear regression model for SNP "rs2645091" vs phenotypic values contained in the qt.phe file.  
2. `assignment2` includes few R scripts and batch scripts to process  hapmap1_nomissing_AF.map/ped files and calculate pvalues using linear regration model to plot manhattan.
