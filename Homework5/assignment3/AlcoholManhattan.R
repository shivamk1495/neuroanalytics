## Summary
# Below 'R' script reads the dataset which represents the associations between
# genetic variants and frequency of drinking alcohol from the UK Biobank project
# This Script use the dataset of “Frequency of Drinking Alcohol” of both sexes
# and plot all SNPs on chromosome 4 with P<1x10^-4 using ggplot2.

# importing "tidyverse" library
options(stringsASFactors = FALSE)

library("tidyverse")

smallest_pval <- 0.0001

raw_alcohol_data <- read_tsv("20414.gwas.imputed_v3.both_sexes.tsv")
# read_tsv() function is used to read this file.

filtered_data <- raw_alcohol_data[raw_alcohol_data["pval"] <= smallest_pval, ]
# identify which chromosome contains the most strongly associated SNP
# with frequency of drinking alcohol.

nomissing_pval <- filtered_data[!is.na(filtered_data$pval), ]

detailed_first <- as.data.frame(
    do.call(rbind, str_split(nomissing_pval$variant, ":", n = 4))
# str_split() function is used to split up the 1st column into constituent parts
)

cleaned_data <- cbind(detailed_first, nomissing_pval)

fresh_data <- cleaned_data[cleaned_data$V1 != "X", ]
fresh_data <- transform(fresh_data, pval = -log10(pval))

pdf("AlcoholManhattan.pdf")
# initiating a pdf to store graphs

plt <- ggplot(data = fresh_data) + geom_jitter(
    aes(x = factor(V1, levels = unique(fresh_data$V1)),
    y = pval, color = V1)
)
# Ploting all SNPs on that chromosome with P<1x10^-4 using ggplot2.
plt + labs(x = "Chromosome", y = "P-value")
dev.off() # saving.
