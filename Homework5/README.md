# Homework4

Homework5 directory contains four assignments. These are based on topics such as ggplot and using a dataset and plotting that data in a specific way.

Below are few command/symbols which are used in these assignments.

* "read.table" for reading any space delemited files like qt.phe/.map.
* "tidyverse" for performing few operation like uniting (unite), merging of data.
* "ggplot2" for ploting data.

---

## assignment1

This contains a `phenohisto.R` files which is used to make a histogram of the phenotype values found in the qt.phe.

---

## assignment2

This assignment is much similar to the `assignment1` whereas in `assignment2` we are making a histogram of the minor allele frequencies from all the SNPs in hapmap1_nomissing_AF.ped/.map.

---

## assignment3

This assignment uses a dataset of frequency of drinking alcohol from the UK Biobank project. Identifing the chromosome  contains the most strongly associated SNP (meaning smallest p-value) ssing tidy functions. After that all SNPs on that chromosome with P<1x10-4 were plotted using ggplot2. A useful function str_split() is also used for spliting up the 1st column into its constituent parts.

## assignment4

In a paragraph, describe what I will do for my final assignment.