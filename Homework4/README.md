# Homework4

Homework4 directory contains three assignments. These are based on topics like cleaning of bad data, filtering based on few parameters like frequency of alleles.

Below are few command/symbols which are used in these assignments.

* "read.table" for reading any space delemited files like .ped/.map.
* "tidyverse" for performing few operation like uniting (unite), merging of data.
* "ggplot2" for ploting geneotype and phenotype data.

---

## assignment1

This contains a `missingnessQC.R` files which is used to remove SNPs with a high degree of missingness.

---

## assignment2

This assignment is much similar to the `assignment1` whereas in `assignment2` we are filtering alleles whose frequency is less than 5% or greater than 95%.

---

## assignment3

This plots several SNPs on x-axis such as homozygous for allele 1, heterozygous, homozygous for allele 2. It will generate hundreds of SNPs graph in a pdf file, every SNP plots all the 'subjectId.Phenotype' on y-axis and different SNPs on x-axis for nomissing threshold as well as alele frequency greater than 95% or less than 5% data.
