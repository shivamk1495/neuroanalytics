# Homework3

Homework3 directory contains two assignments. These are based on topics like data plotting, organizing data, naming data files by using a simple R scripts.

Below are few command/symbols which are used in these assignments.

* "if, elif, fi" for writing conditional statements in bash script
* "chmod" for changing the permissions of files & directories.
* "seq" for generating the sequence of numbers.
* "mkdir, mv" for performing few operations on files.
* "ggplot" for ploting the data.

---

## assignment 1

this contains a `longitudinaldata.pdf` files which represent graphs from the Homework 2 assignment 2 fakedata gernated file according to 100 Donor data file and `README` file. The file I created by assignment2 script to generate raw data as random numbers for 100 donors at each of 10 time points. Created a R script to plot all of this longitudinal data for each donor. For each donor, I have ploted each time point on the x-axis and the phenotype on the y-axis as a scatterplot. For each donor, each of the 10 time points will have 5 data points (dots). Created a PDF file called longitudinaldata.pdf that contains 100 plots, one for each donor. The x and y axes are labeled and the donor number is the title of each plot.

---

## assignment2

This assignment is to plot a file called hapmap1.zip that contains some genotype and phenotype data. The genotype data is in the file 'hapmap1.ped', where the first 6 columns describe the individual being genotyped and then every two columns after that describe the genotype at a SNP. The genotype at the SNP is either 0 0 (missing genotype), 1 1 (homozygous for allele 1), 1 2 (heterozygous), or 2 2 (homozygous for allele 2). For each subject, there is also a phenotype given in 'qt.phe' where the first column is the subject ID and the third column is the phenotype value. The SNPs in the '.ped' file are in the same order as the rows in the 'hapmap1.map' file, meaning that the first SNP in the '.ped' file (columns 7 and 8) corresponds to the first row, second column of the '.map' file. For the first 100 SNPs, plot the genotype on the x-axis as homozygous for allele 1, heterozygous, homozygous for allele 2 (in that order) versus the phenotype on the y-axis. The title of each plot have the SNP name. this plots are saved in a PDF file called "genotypeassociation.pdf".
