## Summary
# Below 'R' script reads the sample donor files from HomeWork2 assignment2
# and generate scatter plots, these scatter plots displays one plot for one
# donors 10 time points data. The x-axis holds the 'Time Points' and y-axis
# have random timepoint value. The function "pdf('filename_address')" is used to
# create a new pdf file using filename at given address.

# The "read.csv('filename')" reads the "donor*_tp*.txt" file
# and returns a data.frame, all time points are merged using
# "cbind(df1, df2)" method vertically. The ploting of data
# has done using "plot()" method, where 'time_points' and
# 'random_values' (random data) are passed to x-axis and y-axis respectively.

options(stringsASFactors = FALSE)

setwd("/nas/longleaf/home/skaushik/neuroanalytics/Homework2/assignment2/fakedata") 
# setting working directory to fakedata folder

print(paste("Working Directory", getwd()))

pdf("/nas/longleaf/home/skaushik/neuroanalytics/Homework3/assignment1/longitudinaldata.pdf")
# define the path to where I want to store the file,

for (i in seq(1, 100)) { # loop for reading 100 donors file

  if (i <= 9) {
    donor_number <- paste("00", i, sep = "")
  }
  else if (i <= 99) {
    donor_number <- paste("0", i, sep = "")
  }
  else {
    donor_number <- paste(i, sep = "")
  }

  random_data <- NA

  # nested loop for reading time points file of each donor
  for (j in seq(1, 10)) {

    if (j <= 9) {
      time_point <- paste("00", j, sep = "")
    }
    else{
      time_point <- paste("0", j, sep = "")
    }

    file_name <- paste(
      "donor", i, "/donor", donor_number, "_tp", time_point, ".txt", sep = ""
    ) # making relative path to time point file.

    phenotype <- read.csv(file = file_name) # reading 'time point' file
    tp <- j

    if (any(is.na(random_data))) { # checking if the first time point is read.
      random_data <- cbind(tp, phenotype) # vertical merging of time points
    }
    else { # else for not the first time point read
      random_data <- rbind(random_data, cbind(tp, phenotype)) # v-merging
    }

  }

  time_points <- random_data[["tp"]]
  random_values <- random_data[["data"]]

  plot(time_points, random_values, xlab = "Time Point", ylab = "Phenotype", main = paste("Donor - ", i))
}
print("PDF file is generated!!!")
dev.off()
