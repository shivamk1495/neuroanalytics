# Homework2

Homework2 directory contains three assignments. These are based on topics like git repository, organizing data, naming data files to sort properly with `ls` command and a simple R script for printing a message.

Below are few command/symbols which are used in these assignments.

* "if, elif, fi" for writing conditional statements in bash script
* "chmod" for changing the permissions of files & directories.
* "seq" for generating the sequence of numbers.
* "mkdir, mv" for performing few operations on files.

---

## assignment1

This contains a `assignment1.txt` files which explains a bit about the `.gitignore` and `README` file and also gives few examples.

---

## assignment2

This assignment is much similar to the `assignment3` of `Homewor1` whereas in `assignment3` when `ls` command was ran after generating sample donors data files, the order of files was not proper. In this `assignment2`, the name of the sample donors data files is tweaked a bit so that `ls` command gives a proper order of files. The name of donor data files is changed to something like `donor*_tp*.txt`, where `*` refers to number with few leading zeros to make it 3 digit.
This assignment also organize the files to store them in `fakedata/donor*` directory to not mess up the `assignment2` directory.

## assignment3

This is a simple R script which give a message to 'Jordan' by echoing it into console. Here defining a variable `message` is done at first line and then print fucntion is used to print that variable value in the console.
