#!/bin/bash

# Summary
## Below script generates a series of files using two nested for loops.
## "seq" used for printing a sequence of numbers.
## "-ne" - "-n" used for do not output the trailing newline, where "-e"is used for enabling interpretation of backslash escapes.
## "\n" is used to add the next line.

for i in $(seq 1 100); do
    for j in $(seq 1 10); do
        filename="donor" ## prefix 'donor' is assigned to variable name 'filename'

        # Below 'if' conditions runs for donor numbers which is why 'i' is used
        if ((1 <= $i && $i <= 9)); then     ## if donor number exists b/w 1 and 9 included
            filename="${filename}00${i}"    ## append '00${i}' ('i' is donornumber) suffix to 'filename'
        elif ((10 <= $i && $i <= 99)); then ## if donor number exists b/w 10 and 99 included
            filename="${filename}0${i}"     ## append '0${i}' suffix to 'filename'
        else                                ## otherwise this will run only for '100' donor
            filename="${filename}${i}"      ## append only '${i}' suffix
        fi

        # Below 'if' conditions runs for time points numbers which is why 'j' is used
        if ((1 <= $j && $j <= 9)); then         ## if time point exists b/w 1 and 9 included
            filename="${filename}_tp00${j}.txt" ## append 'tp00${j}.txt' ('j' is time point) suffix to 'filename'
        elif (($j == 10)); then                 ## if time point equals to 10
            filename="${filename}_tp0${j}.txt"  ## append 'tp0${j}.txt' suffix to 'filename'
        fi
        echo -ne "data\n$RANDOM\n$RANDOM\n$RANDOM\n$RANDOM\n$RANDOM\n" >"${filename}"
        ## this 'echo' generates the file data and name it using 'filename' variable.
    done
done

####################################   


# summary 
## The below script is used to organize messy files 
## filepath variable is used to pick files from the assignment2 directory. 
## This will read all the donor files from the assignment2 directory and create a donors directory in fakedata directory.
## Later using 'ls' command it will pick all the files of one donor and move those files with the help of 'mv' command.

filepath="/nas/longleaf/home/skaushik/neuroanalytics/Homework2/assignment2" ## assigned a variable for the file path.

for i in $(seq 100 -1 1); ## this time seq is running in reverse order starting from 100 and ends on 1.
do 

$(mkdir -p fakedata/donor${i}); ## creates fakedata parent directory and respective donors directory.

filename="donor"
# Below 'if' conditions runs for donor numbers which is why 'i' is used
        if ((1 <= $i && $i <= 9)); then     ## if donor number exists b/w 1 and 9 included
            filename="${filename}00${i}"    ## append '00${i}' ('i' is donornumber) suffix to 'filename'
        elif ((10 <= $i && $i <= 99)); then ## if donor number exists b/w 10 and 99 included
            filename="${filename}0${i}"     ## append '0${i}' suffix to 'filename'
        else                                ## otherwise this will run only for '100' donor
            filename="${filename}${i}"      ## append only '${i}' suffix
        fi

donorfiles=$(ls $filepath/${filename}*.txt);

    for file in $donorfiles; ## this loop will put each donor’s 10 files into their own directory.
    do 

    $(chmod -w $file); ## Make file read only so that it cannot be accidentally edited.
    $(mv $file fakedata/donor${i}/$(basename $file)) ## move file to fakedata.

    done
done 