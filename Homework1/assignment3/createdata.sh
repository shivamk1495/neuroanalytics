#!/bin/bash

# Summary
## Below script generates a series of files using two nested for loops.
## "seq" used for printing a sequence of numbers.
## "-ne" - "-n" used for do not output the trailing newline, where "-e"is used for enabling interpretation of backslash escapes.
## "\n" is used to add the next line.

for i in $(seq 1 50); do
    for j in $(seq 1 10); do
        echo -ne "data\n$RANDOM\n$RANDOM\n$RANDOM\n$RANDOM\n$RANDOM\n" >"donor${i}_tp${j}.txt"
        ## output will be generated in the form of .txt file named with "donor${i}_tp${j}.txt"
    done
done
