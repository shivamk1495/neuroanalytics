#!/bin/bash

# summary 
## The below script is used to organize messy files created by problem3. 
## filepath variable is used to pick files from the assignment3 directory. 
## This will read all the donor files from the assignment3 directory and create a donors directory in fakedata directory.
## Later using 'ls' command it will pick all the files of one donor and move those files with the help of 'mv' command.

filepath="/nas/longleaf/home/skaushik/assignments/assignment3" ## assigned a variable for the file path.

for i in $(seq 50 -1 1); ## this time seq is running in reverse order starting from 50 and ends on 1.
do 

$(mkdir -p fakedata/donor${i}); ## creates fakedata parent directory and respective donors directory.
donorfiles=$(ls $filepath/donor${i}*.txt);

    for file in $donorfiles; ## this loop will put each donor’s 10 files into their own directory.
    do 

    $(chmod -w $file); ## Make file read only so that it cannot be accidentally edited.
    $(mv $file fakedata/donor${i}/$(basename $file)) ## move file to fakedata.

    done
done 