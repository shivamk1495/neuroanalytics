# Homework1

Homework1 directory holds four assignments. These are based on topics like pattern making, file reading, file creation and organizations.

Below are few command/symbols which are used in these assignments.

* "\r" for printing from start of line.
* "\b" for backspace one character.
* "\n " for adding enter (next line).
* "wc" for counting characters in a file.
* "seq" for generating a sequence of numbers in a given range.
* "ls", "mv", "mkdir" for several files operations.

---

## assignment1

A `swirl.sh` script creates a nice illusion of loading/processing symbol, which rotates in several directions at one position, with time help of `echo` statements and `for` loop. This is a very common pattern which is used in several scripts which are time consuming.

---

## assignment2

A `thegeneticsmatrix.sh` script reads a `hapmap1.ped` data file and echo one line at a time. This also sleeps for 1s while printing and instead of printing whole line it chops it using `cut` command. Chopping helps to print it in a nice matrix format, which is again more readable and understandable.

---

## assignment3

Creates a sample data for 50 donors with `createdata.sh` script using `for` loop, `RANDOM` and `>` (output redirection) symbol. The naming convention for naming donor files is `donor*_tp*.txt`, where `*` represents some number.

---

## assignment4

While generating sample donors data files in the above assignment. The `createdata.sh` created a lot of sample files and it looks messy in a single directory so, `organizedata.sh` helps in organizing this messy data. `organizedata.sh` creates `fakedata` directory and inside that it creates `donor*` directory, where it stores all time points data file for each donor in its directory.
