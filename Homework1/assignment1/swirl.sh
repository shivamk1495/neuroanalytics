#!/bin/bash

# Summary 
## Below program will spin one spinner in Anticlockwise direction.
## "\r" is used for printing from start of line.
## "\b" is used for backspace one character and "\b\b" for backspace two characters in this script.
## "\n " used for enter (next line).

# to make spinner spin in other direction "\"(back slash) is used.
# to make the spinner spin faster "0.5" sleep time is reduced.

echo -ne '\\\\'; ## this echo will print 2 backslash.
sleep 0.5; ## It will sleep for 0.500 ms.
echo -ne '\b\b=='; ## this echo will print 2 equals.
sleep 0.5;
echo -ne '\b\b//'; ## this echo will print 2 forwardslash.
sleep 0.5;
echo -ne '\b\b||'; ## It will print 2 Pipe symbols.
sleep 0.5;
echo -ne '\b\b\\\\'; 
sleep 0.5;
echo -ne '\b\b==';
sleep 0.5;
echo -ne '\b\b//';
sleep 0.5;
echo -ne '\b\b||';
echo -ne '\r  \n'; # this will overwrite "||" with "  ", and then enter.  

##############################

# Summary 
## Below program will spin two spinner separated with tab, both will run in opposite directions (Clockwise and Anticlockwise).
## "\r" is used for printing from start of line Whereas "\t" is used for tab in this script.

# for lope is used for running 0.50 iterations using "seq command".
# to make spinner spin in other direction "\"(back slash) is used.
# to make the spinner spin faster "0.05" sleep time is reduced.
 
for i in $(seq 10);

do

echo -ne '\\\\\t//'; ## this echo will print 2 backslash and 2 forwardslash separated with tab.
sleep 0.05; ## It will sleep for 0.500 ms.
echo -ne '\r==\t=='; ## this echo will print 4 equal-to, pairs separated with tab.
sleep 0.05;
echo -ne '\r//\t\\\\'; ## this echo will print 2 forwardslash and 2 backslash separated with tab.
sleep 0.05;
echo -ne '\r||\t||'; ## It will print 2 Pairs of Pipe symbols separated with tab.
sleep 0.05;
echo -ne '\r\\\\\t//'; 
sleep 0.05;
echo -ne '\r==\t==';
sleep 0.05;
echo -ne '\r//\t\\\\';
sleep 0.05;
echo -ne '\r||\t||\r'; 

done
 ## done represent the end of for loop. 