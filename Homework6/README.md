# Homework6

Homework6 directory contains two assignments. These are based on topics such as sbatch (Simple Linux Utility for Resource Management (SLURM)) scripting and using a dataset and plotting that data in a specific way.

Below are few command/symbols which are used in these assignments.

* "read.table" for reading any space delemited files like qt.phe/.map.
* "tidyverse" for performing few operation like uniting (unite), merging of data.
* "ggplot2" for ploting data.
* "sbatch" for scheduling the jobs.
* "squeue" for checking the running jobs.

---

## assignment1

This contains couple of R scripts and batch (Slurm) script that generates a histogram after runnning linear regression model for SNP "rs2645091" vs phenotypic values contained in the qt.phe file.  

---

## assignment2

This assignment is much similar to the `assignment1` whereas in `assignment2` R scripts and batch scripts to process  hapmap1_nomissing_AF.map/ped files and calculate pvalues using linear regration model to plot manhattan.
