#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --job-name="Running Permutation Tests"
#SBATCH --output=permutation-test.out

cd /nas/longleaf/home/skaushik/neuroanalytics/Homework6/assignment1

module load R/4.1.0

Rscript --vanilla drivedata.R

for i in $(seq 1 100); do
    Rscript --vanilla runpermutationtest.R "$i"
done

Rscript --vanilla cleanup.R