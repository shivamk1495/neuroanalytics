#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --job-name="Conducting Association"
#SBATCH --output=manhattan-plot.out

cd /nas/longleaf/home/skaushik/neuroanalytics/Homework6/assignment2

module load R

Rscript --vanilla drivedata.R

rm rawdata.ped

for i in $(seq 1 "$(ls | grep -c "nomissing_af_data")"); do
    Rscript --vanilla conductassociation.R "$i"
done

Rscript --vanilla manhattanplot.R
Rscript --vanilla cleanup.R