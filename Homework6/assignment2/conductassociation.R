
options(stringsASFactors = FALSE)

args <- commandArgs(trailingOnly = TRUE)

nomissing_afmap <- read.table("hapmap1_nomissing_AF.map") # reading map file
rsframe <- read.table(paste("nomissing_af_data", args[1], sep = "_"))

# initializing empty dataframe to store chromosome with its p-val
pvalframe <- data.frame(matrix(ncol = 2, nrow = 0))
row_names <- c("chromosome", "pval")
colnames(pvalframe) <- row_names

# looping over all SNP to fetch chromosome and calculate pval
for (i in seq(1, ncol(rsframe) - 1)) {
    chromosome <- nomissing_afmap[
        which(nomissing_afmap["V2"] == colnames(rsframe[i])), "V1"
    ]
    modeldata <- lm(rsframe[, i]~rsframe$values) # running lm model
    pval <- pf(
        summary(modeldata)$fstatistic[1],
        summary(modeldata)$fstatistic[2],
        summary(modeldata)$fstatistic[3],
        lower.tail = FALSE
    )
    onesnp <- data.frame(chromosome = chromosome, pval = pval)
    rownames(onesnp) <- colnames(rsframe[i])
    pvalframe <- rbind(pvalframe, onesnp)
}

# transforming pval to -log10
pvalframe <- transform(pvalframe, pval = -log10(pval))

# Save data to generate manhattan plot
write.table(pvalframe, file = "rawdata.ped", append = TRUE, col.names = FALSE,
    quote = FALSE)
