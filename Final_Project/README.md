# final project

final project directory holds three scripts.

Below are few command/symbols which are used in these assignments.

* "\r" for printing from start of line.
* "\b" for backspace one character.
* "\n " for adding enter (next line).
* "wc" for counting characters in a file.
* "seq" for generating a sequence of numbers in a given range.
* "ls", "mv", "mkdir" for several files operations.

---

## files_contain

File sizes are reduced in order to reduce the size of large files that are uploaded.
